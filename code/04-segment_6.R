
# Parameters --------------------------------------------------------------

n_periods <- 6

cutoff_dates_segmentation_6 <- ymd(cutoff_date)

# Calculate summary for each period ---------------------------------------

sheet_6_periods <- map(
  cutoff_dates_segmentation_6,
  ~ calculate_aggregations(
    portfolio_by_period_pro,
    input_n_periods = n_periods,
    input_cutoff_date = .
  )
)

sheet_6_periods %<>%
  set_names(cutoff_dates_segmentation_6)

# Segmentation ------------------------------------------------------------

categorized_data_6 <- map(
  sheet_6_periods,
  ~ .[["RESUMEN_N_PERIODO"]] %>%
    rename(CEI_ASOCIADO_ALT = CEI_ASOCIADO, CEI_ASOCIADO = CEI_ASOCIADO_2) %>%
    categorize_key_var()
)

segmented_data_6 <- map(
  categorized_data_6,
  ~ assign_segments(., input_n_periods = n_periods)
)

segmented_data_support_6 <- map2(
  .x = segmented_data_6,
  .y = as_date(names(segmented_data_6)),
  .f = ~ consolidate_segments(
    input_data = .x,
    input_cutoff_date = .y,
    input_bank = managed_by_bank_pro,
    input_inactive = inactive_pro
  )
)

# Export binary data ------------------------------------------------------

save(
  sheet_6_periods,
  categorized_data_6,
  segmented_data_6,
  segmented_data_support_6,
  file = glue("./output/segments_6_{cut_off_period}.RData")
)

## This makes sure that R loads the workflowr package
## automatically, everytime the project is loaded
if (requireNamespace("workflowr", quietly = TRUE)) {
  message("Loading .Rprofile for the current workflowr project")
  library("workflowr")
} else {
  message("workflowr package not installed, please run install.packages(\"workflowr\") to use the workflowr functions")
}

# Set common options
options(
  digits = 4,
  prompt = "R> ",
  show.signif.stars = FALSE,
  continue = " ",
  warnPartialMatchArgs = TRUE,
  warnPartialMatchDollar = TRUE,
  usethis.full_name = "Juan Felipe Padilla Sepulveda",
  usethis.description = list(
    `Authors@R` = 'person("Juan Felipe", "Padilla Sepulveda",
    email = "juanf_padilla@coomeva.com.co", role = c("aut", "cre"))',
    License = "MIT + file LICENSE",
    Version = "0.0.0.9000"
  ),
  usethis.protocol  = "ssh"
)

if (interactive()) {

  # Developer packages
  suppressMessages(require(devtools))
  suppressMessages(require(testthat))

  # Quotes
  suppressWarnings(try(fortunes::fortune(), silent = TRUE))
}

# Startup functions
.First <- function() {
  if (interactive()) require(conflicted)

  message("\nWelcome at ", format(Sys.time(), '%B %Y'), "\n")
}

# Session end function
.Last <- function() {
  cond = suppressWarnings(!require(fortunes, quietly = TRUE))
  if (cond) try(install.packages("fortunes"), silent = TRUE)

  message("\nGoodbye at ", format(Sys.time(), '%B %Y'), "\n")
}
